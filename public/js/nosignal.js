const noSignal = () => new p5((s) => {
    console.log("no Signal running...")

    if(sound != undefined){
        sound.unload()
        playBtn.style.display = 'none'
        pauseBtn.style.display = 'none'
        sub.style.display = 'none'
    }

    let offWhite = '#eeeeff'

    let bars = 4
    let btwn = 5
    let barW = 20

    let xoff = 0.0
    let n = []

    s.setup = function() {
        bg = s.createCanvas(s.windowWidth, s.windowHeight)
        bg.id('bg-cnv')
        title.innerHTML = 'No Signal…'
    }

    s.draw = function() {
        s.background(0, 50, 200)
        s.noStroke()
        s.translate(s.windowWidth / 2, s.windowHeight / 2 - 30)
        s.fill(offWhite)

        xoff = xoff + 0.005
        for (let i = 0; i <= bars; i++) {
            n[i] = s.noise(xoff + i / 10)
        }

        s.rect((-barW - btwn) * 2, 0, barW, -40 * n[0])
        s.rect(-barW - btwn, 0, barW, -50 * n[1])
        s.rect(0, 0, barW, -70 * n[2])
        s.rect(barW + btwn, 0, barW, -80 * n[3])

    }

    s.windowResized = function() {
        s.resizeCanvas(s.windowWidth, s.windowHeight)
    }

})