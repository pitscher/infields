const playfile = (fileNum) => new p5((s) => {
    document.getElementById('p5_loading').style.display = ''

    //let button
    let audio
    let img

    s.preload = function () {
        console.log('loading audio...')
        s.soundFormats('ogg', 'mp3')
        audio = s.loadSound('audio/' + fileNum + '.mp3')
        img = s.loadImage('img/' + fileNum + '.jpg')
    }

    s.setup = function () {
        s.noCanvas()
        bg = s.createCanvas(s.windowWidth, s.windowHeight)
        bg.id('bg-cnv')
        console.log('audio playing!')
        audio.play()
        audio.loop()

        //s.imageMode(CENTER)
        //s.image(img, s.windowWidth/2, s.windowHeight/2)
        s.noStroke()

        //button = s.createButton('&#x25B6;')
        //button.addClass('playpause')
        //button.mousePressed(togglePlay)

        //fft = new p5.FFT(0.3, 64);
    }

    let posX = 0
    let posY = 0
    s.draw = function () {
        if (s.frameCount % 10 == 0) {
            /*
            let spectrum = fft.analyze()
            let getSpec = s.map(s.winMouseX, 0, s.windowWidth, 0, spectrum.length)
            let _spec = spectrum[getSpec]
            let amp = s.map(_spec, 0, 255, 0, 2)
            console.log("spec: "+ _spec + "amp: "+ amp)
*/
            s.fill(0,0,200)
            s.rect(0,0, s.windowWidth, s.windowHeight)
            let rectX = s.windowWidth / 10;
            let rectY = rectX
            for (let x = 0; x <= s.windowWidth; x += rectX) {
                for (let y = 0; y <= s.windowHeight; y += rectY) {
                    s.image(img, x, y, rectX, rectY, s.random(img.width), s.random(img.height), rectX*2, rectY*2)
                }
            }
            /*
            if (posX <= s.windowWidth) {
                posX += rectX
            } else {
                posY += rectY;
                posX = 0;
            }
            if(posY >= s.windowHeight) posY = 0
            console.log(posX)
            console.log(posY)
            s.image(img, posX, posY, rectX, rectY, random(img.width), random(img.height), rectX, rectY)
           */
        }


    }

    s.windowResized = function () {
        s.resizeCanvas(s.windowWidth, s.windowHeight)
    }

    togglePlay = function () {
        if (audio.isPlaying()) {
            button.html('&#x25A0;')
            audio.pause();
            s.background(255, 0, 0)
        } else {
            button.html('&#x25B6;')
            audio.play();
            s.background(0, 255, 0)
        }
    }

})