const playfile = (fileNum) => {

    if(document.getElementById('bg-cnv') != null){
        document.getElementById('bg-cnv').style.display = 'none'
    }

    if(sound != undefined){
        sound.unload()
        playBtn.style.display = 'none'
        pauseBtn.style.display = 'none'
    }

    loading.style.display = 'block'
    sub.style.display = 'block'
    sub.innerHTML = 'loading soundfield...'

    sound = new Howl({
        src: [`./audio/${fileNum}.webm`, `./audio/${fileNum}.mp3`],
        html5: true,
        autoplay: true,
        loop: true,
        onend: function () {
            console.log('➰looped➰')
        },
        onplay: function () {
            playBtn.style.display = 'none'
            pauseBtn.style.display = 'block'
            sub.innerHTML = ''
        },
        onpause: function () {
            playBtn.style.display = 'block'
            pauseBtn.style.display = 'none'
        },
        onload: function(){
            loading.style.display = 'none'
            sub.innerHTML = ''
            playBtn.style.display = 'block'
            title.innerHTML = 'FIELD #' + fileNum
        },
        onplayerror: function () {
            sub.innerHTML = "press play"
            sound.loop('unlock', function () {
                sound.play();
            });
        }
    })

    sound.play()



}

function toggleSound() {
    if (sound.playing()) {
        sound.pause()
    } else {
        sound.play()
    }
}